<!DOCTYPE html>
<html lang="fi">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="IT-Palvelut Norén on Porissa toimiva IT-alan yritys. Palveluitamme ovat mm. yrityksen IT-ratkaisut, laitteet, kattava ja luotettava ylläpito, tuki ja huolto, konsultointi, sekä niin valmiit, kuin räätälöidytkin ohjelmistot. Kysy Lisää!">

    <title>IT-Palvelut Norén Oy</title>
    <link rel="icon" type="image/png" href="favicon.png">

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,300,300italic,400italic,500,600,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <!-- Plugin CSS -->
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/style.min.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <!--<a class="navbar-brand js-scroll-trigger" href="#page-top">Start Bootstrap</a>-->
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="#page-top"><img class="brand-logo-s" src="img/favicon-<?= v6 ?>.png"></a>
              </li>
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="#services">Palvelut</a>
              </li>
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="#support">Etätuki</a>
              </li>
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="#contact">Yhteystiedot</a>
              </li>
          </ul>
        </div>
      </div>
    </nav>

    <header class="logo-container" style="display:block;width:100%;background:#fff;text-align:center;">
        <img src="img/logo-<?= v6 ?>.png">

    </header>

    <section id="services">

      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box mt-5 mx-auto">
              <i class="fa fa-4x fa-lightbulb-o text-primary mb-3 sr-icons"></i>
              <h3 class="mb-3">IT-Ratkaisut</h3>
              <!--<p class="text-muted mb-0">Yrityksen koko IT-infrastruktuuri avaimet käteen-periaatteella.</p>-->
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box mt-5 mx-auto">
              <i class="fa fa-4x fa-laptop text-primary mb-3 sr-icons"></i>
              <h3 class="mb-3">Kaikki laitteet</h3>
              <!--<p class="text-muted mb-0">Työasemien, palvelimien, aktiivilaitteiden, sekä muiden oheislaitteet myynti ja huolto.</p>-->
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box mt-5 mx-auto">
              <i class="fa fa-4x fa-database text-primary mb-3 sr-icons"></i>
              <h3 class="mb-3">Ylläpito</h3>
              <!--<p class="text-muted mb-0">Liiketoiminnan kannalta kriittisten laitteiden ja sovellusten reaaliaikainen ylläpito.</p>-->
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box mt-5 mx-auto">
              <i class="fa fa-4x fa-wrench text-primary mb-3 sr-icons"></i>
              <h3 class="mb-3">Tuki & Huolto</h3>
              <!--<p class="text-muted mb-0">Työasemien, palvelimien, aktiivilaitteiden, sekä muiden oheislaitteet myynti ja huolto.</p>-->
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-6 text-center">
              <div class="service-box mt-5 mx-auto">
                <i class="fa fa-4x fa-comments-o text-primary mb-3 sr-icons"></i>
                <h3 class="mb-3">Konsultointi</h3>
                <!--<p class="text-muted mb-0">Liiketoiminnan kannalta kriittisten laitteiden ja sovellusten reaaliaikainen ylläpito.</p>-->
              </div>
            </div>
            <div class="col-lg-3 col-md-6 text-center">
              <div class="service-box mt-5 mx-auto">
                <i class="fa fa-4x fa-windows text-primary mb-3 sr-icons"></i>
                <h3 class="mb-3">Valmiit ohjelmistot</h3>
                <!--<p class="text-muted mb-0">Valmiista ohjelmistoratkaisuista aina kokonaisiin, täysin yrityksen tarpeisiin räätälöityihin ohjelmistoprojekteihin.</p>-->
              </div>
            </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box mt-5 mx-auto">
              <i class="fa fa-4x fa-code text-primary mb-3 sr-icons"></i>
              <h3 class="mb-3">Räätälöidyt ohjelmistot</h3>
              <!--<p class="text-muted mb-0">Yrityksen koko IT-infrastruktuuri avaimet käteen-periaatteella.</p>-->
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box mt-5 mx-auto">
              <i class="fa fa-4x fa-wordpress text-primary mb-3 sr-icons"></i>
              <h3 class="mb-3">Verkkosivustot</h3>
              <!--<p class="text-muted mb-0">Valmiista ohjelmistoratkaisuista aina kokonaisiin, täysin yrityksen tarpeisiin räätälöityihin ohjelmistoprojekteihin.</p>-->
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-lg-12 text-center">
              <div class="service-box mt-5 mx-auto">
                <h3 class="mb-3 text-muted">Ota yhteyttä niin kerromme palveluista tarkemmin.</h3>
                <a class="btn btn-cta btn-xl text-uppercase js-scroll-trigger" href="#contact">Yhteystiedot</a>
              </div>
            </div>
        </div>
      </div>
    </section>

    <section class="bg-primary" id="support">
      <div class="container">
          <h2 class="section-heading text-center">TeamViewer - Etätuki</h2>
          <hr class="my-4">
        <div class="row">
            <div class="col-lg-4">
                <img src="img/etatuki2.png">
            </div>
          <div class="col-lg-8">
              <div class="o-list">
                  <span class="o-list-index">1</span>
                  <span class="o-list-content"><strong>Lataa</strong> etätuki-ohjelmisto latausnappulaa painamalla</span>
              </div>
              <div class="o-list">
                  <span class="o-list-index">2</span>
                  <span class="o-list-content"><strong>Käynnistä</strong> äsken ladattu ohjelmisto (tuki.exe)</span>
              </div>
              <div class="o-list">
                  <span class="o-list-index">3</span>
                  <span class="o-list-content"><strong>Ilmoita</strong> meille ohjelman generoima ID-tunniste</span>
              </div>
              <div class="o-list">
                  <a class="btn btn-light btn-xl" href="dl/tuki.exe" download>Lataa etätuki</a>
              </div>
          </div>

        </div>
      </div>
    </section>

    <section id="contact">
        <div class="container">
          <div class="row">
            <div class="col-lg-12 text-center">
              <h2 class="section-heading text-uppercase">Yhteystiedot</h2>
              <hr class="my-4">
            </div>
          </div>
          <div class="row team-grid">
            <div class="col-sm-3">
              <div class="team-member">
                <img class="mx-auto rounded-circle" src="img/team.png" alt="">
                <h4>Teemu Norén</h4>
                <p class="text-muted">CEO</p>
                <p class="text-muted">teemu@dano.fi</p>
                <p class="text-muted">+358 40 838 9700</p>
                <ul class="list-inline social-buttons">
                  <li class="list-inline-item">
                    <a href="https://fi.linkedin.com/in/teemu-nor%C3%A9n-51a2b4b5" target="_blank">
                      <i class="fa fa-linkedin"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="team-member">
                <img class="mx-auto rounded-circle" src="img/cloud.png" alt="">
                <h4>Tuomas Norén</h4>
                <p class="text-muted">IT-Specialist</p>
                <p class="text-muted">tuomas@dano.fi</p>
                <p class="text-muted">+358 40 734 8929</p>
                <ul class="list-inline social-buttons">
                  <li class="list-inline-item">
                    <a href="https://www.linkedin.com/in/tuomas-norén-5a222370" target="_blank">
                      <i class="fa fa-linkedin"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="team-member">
                <img class="mx-auto rounded-circle" src="img/code.png" alt="">
                <h4>Jani Suista</h4>
                <p class="text-muted">Software Engineer</p>
                <p class="text-muted">jani@dano.fi</p>
                <p class="text-muted">+358 50 440 9208</p>
                <ul class="list-inline social-buttons">
                  <li class="list-inline-item">
                    <a href="https://www.linkedin.com/in/janisuista/" target="_blank">
                      <i class="fa fa-linkedin"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="team-member">
                <img class="mx-auto rounded-circle" src="img/team.png" alt="">
                <h4>Kirsi Rantala</h4>
                <p class="text-muted">Accounting</p>
                <p class="text-muted">kirsi@dano.fi</p>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12 text-center">
              <h2 class="section-heading text-uppercase">Yritys</h2>
              <hr class="my-4">
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12 text-center">
                <div class="team-member">
                    <h4>Postiosoite</h4>
                    <p class="text-muted">Kukkulansyrjä 1, 28220 Pori</p>
                    <h4>Käyntiosoite</h4>
                    <p class="text-muted">Kanahaukantie 2, Halli 2, 28220 Pori</p>
                    <h4>Y-tunnus</h4>
                    <p class="text-muted">2193148-8</p>
                </div>
            </div>
          </div>
        </div>
    </section>


    <section id="logo-showcase">
        <div class="flexbox-centering">
          <a href="https://www.microsoft.com/fi-fi" target="_blank"><img style="max-width:100%;height:auto;width:600px;" src="img/edu_AEP.png"></a>
        </div>
        <hr class="my-5">
        <div class="flexbox-centering">
          <div class="child"><a href="https://www3.lenovo.com/fi/fi/" target="_blank"><img src="img/lenovo.png"></a></div>
          <div class="child"><a href="https://www.zyxel.com/fi/fi/" target="_blank"><img src="img/zyxel.png"></a></div>
          <div class="child"><a href="http://www8.hp.com/fi/fi/home.html" target="_blank"><img src="img/hp.png"></a></div>
        </div>

    </section>
    <footer id="footer">
      <div class="container text-center">
        <img class="copyright-logo" src="img/logo-v6-hzl.png">
            <!-- Button trigger modal -->
            <a class="pp-link" data-toggle="modal" data-target="#privacy-policy">
              Rekisteriseloste
              </a>
      </div>
  </footer>

    <!-- Modal -->
    <div class="modal fade" id="privacy-policy" tabindex="-1" role="dialog" aria-labelledby="privacy-policyLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
              <img class="copyright-logo" src="img/logo-v6-hzl.png">

            <button type="button" class="close" data-dismiss="modal" aria-label="Sulje">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              <section>
                  <h5 class="modal-title" id="privacy-policyLabel">Rekisteriseloste</h5>
                  <span class="modal-desc">Henkilötietolain (523/99) 10 § mukainen rekisteriseloste</span>
              </section>
              <section>
                  <h6>1. Rekisterinpitäjä</h6>
                  <p>IT-Palvelut Norén Oy</br>Y-tunnus: 2193148-8</p>
              </section>
              <section>
                  <h6>2. Yhteyshenkilö rekisteriä koskevissa asioissa</h6>
                  <p>Teemu Norén</br>0408389700</br>teemu@dano.fi</p>
              </section>
              <section>
                  <h6>3. Rekisterin nimi</h6>
                  <p>IT-Palvelut Norén Oy:n markkinointi- ja analytiikkarekisteri</p>
              </section>
              <section>
                  <h6>4. Henkilötietojen käsittelyn tarkoitus</h6>
                  <p>Rekisterin tarkoituksena on rekisterinpitäjän ja asiakkaan välisen asiakassuhteen hoitaminen, sekä markkinointi ja viestintä.Rekisterin hallinta ja tietojen käsittely voidaan ulkoistaa rekisterinpitäjän yhteistyökumppanille, joka on sitoutunut noudattamaan tätä rekisteriselostetta.</p>
              </section>
              <section>
                  <h6>5. Rekisterin tietosisältö</h6>
                  <p>Rekisteri saattaa sisältää seuraavia tietoja asiakkaasta: </p>
                  <ul>
                      <li>Nimi</li>
                      <li>Puhelinnumero</li>
                      <li>Osoite</li>
                      <li>Sähköpostiosoite</li>
                      <li>Organisaation nimi ja y-tunnus</li>
                      <li>Yhteysloki</li>
                  </ul>
              </section>
              <section>
                  <h6>6. Sääntöjen mukaiset tietolähteet</h6>
                  <p>Rekisteri koostetaan rekisterinpitäjän asiakastietojärjestelmästä sekä sivuston kävijäseurantatyökalusta, yleisesti saatavilla olevista internetlähteistä ja muista julkisista sekä maksuttomista että maksullisista lähteistä.</p>
              </section>
              <section>
                  <h6>7. Tietojen säännönmukaiset luovutukset</h6>
                  <p>Rekisterinpitäjä ei luovuta tietoja ulkopuolisille lukuun ottamatta kohdassa 4 mainittua rekisterinpitäjän yhteistyökumppania, paitsi Suomen viranomaistoimien niin edellyttäessä.</p>
              </section>
              <section>
                  <h6>8. Tietojen siirto EU:n tai ETA:n ulkopuolelle</h6>
                  <p>Henkilötietoja ei pääsääntöisesti siirretä Euroopan unionin tai Euroopan talousalueen ulkopuolelle.</p>
              </section>
              <section>
                  <h6>9. Rekisterin suojauksen periaatteet</h6>
                  <p>Henkilötiedot säilytetään luottamuksellisina. Rekisterinpitäjän ja sen mahdollisten tietotekniikkakumppaneiden tietoverkko ja laitteisto, jolla rekisteri sijaitsee, on suojattu palomuurilla ja muilla tarvittavilla teknisillä toimenpiteillä.</p>
              </section>
              <section>
                  <h6>10. Evästeet</h6>
                  <p>Rekisterinpitäjän sivuilla saatetaan käyttää evästeitä käyttökokemuksen kehittämiseksi. Eväste on tekstitiedosto, joka verkkosivulla käydessä tallentuu käyttäjän tietokoneelle. Se sisältää tietoja ja sitä käytetään mm. markkinointiin ja sivuston kehittämiseen. Evästeiden avulla kerätään tietoa sivujen käytöstä. Käyttäjän henkilöllisyys ei käy ilmi evästeiden avulla. Käyttäjä voi poistaa evästeet käytöstä selaimen asetuksista. Rekisterinpitäjän ei takaa, että sivusto toimii oikein evästeiden poistamisen jälkeen.</p>
              </section>
          </div>
          <div class="modal-footer text-center">
            <a class="btn btn-primary" href="dl/Rekisteriseloste2018.docx" download>Lataa rekisteriseloste</a>
          </div>
        </div>
      </div>
    </div>
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/creative.min.js"></script>

  </body>

</html>
